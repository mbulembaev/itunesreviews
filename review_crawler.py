import requests
import typing
import csv


def get_json(url):
    """
    Returns json response or Returns None if no json found.

    :param url:
        The url to get the json from.
    """
    response = requests.get(url)
    if response.status_code != 200:
        return None
    return response.json()


def get_reviews(app_id, page=1):
    """
    Returns a list of reviews in dictionary

    :param app_id:
        The app_id of application.
    :param page:
        The page id to start the loop.
    """
    reviews: typing.List[dict] = [{}]

    while True:
        url = f'https://itunes.apple.com/ru/rss/customerreviews/id={app_id}/' \
              f'page={page}/sortby=mostrecent/json'
        data_json = get_json(url)

        if not data_json:
            print(f'{len(reviews)} reviews was crawled')
            return reviews

        data_feed = data_json.get('feed')

        if not data_feed.get('entry'):
            get_reviews(app_id, page + 1)

        reviews += [
            {
                'review_id': entry.get('id').get('label'),
                'review_title': entry.get('title').get('label'),
                'review_text': entry.get('content').get('label'),
                'rating': entry.get('im:rating').get('label'),
                'author': entry.get('author').get('name').get('label'),
                'author_url': entry.get('author').get('uri').get('label'),
                'version': entry.get('im:version').get('label'),
            }
            for entry in data_feed.get('entry')
            if not entry.get('im:name')
        ]

        page += 1


def generate_csv(reviews):
    """
    Generate a csv file with reviews

    :param reviews:
        The list of reviews in dictionary
    """
    with open('reviews.csv', mode='w', encoding="utf-8", newline='') as csv_file:
        fieldnames = ['bookmaker_name', 'review_id', 'review_title', 'review_text',
                      'rating', 'author', 'author_url', 'version']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

        writer.writeheader()
        for review in reviews:
            if review:
                writer.writerow({
                    'bookmaker_name': 'Лига ставок',
                    'review_id': review.get('review_id'),
                    'review_title': review.get('review_title'),
                    'review_text': review.get('review_text'),
                    'rating': review.get('rating'),
                    'author': review.get('author'),
                    'author_url': review.get('author_url'),
                    'version': review.get('version')
                })


application_id = 1065803457  # Liga stavok App id
reviews = get_reviews(application_id)
generate_csv(reviews)

